package jp.alhinc.nomura_takashi.calculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CalculateSales {
	List<String> BranchContents = new ArrayList<>();
	List<String> ProductContents = new ArrayList<>();
	List<String> Earnings = new ArrayList<>();
	int i;
	BufferedReader br = null;
	File file;
	public static void main(String[] args) {
		CalculateSales  Sales = new CalculateSales ();
		Sales.openbranch(args[0]);
		Sales.openproduct(args[0]);
		Sales.openrcd(args[0]);
		Sales.totalling(args[0]);
		Sales.createfile(args[0], "/branch.out");
		Sales.createfile(args[0], "/commodity.out");
		System.out.println("終了しました");


	}


	//支店定義ファイル読み込み
	public void openbranch(String path) {
				try {
				    file = new File(path,"branch.lst");
					FileReader fr = new FileReader(file);
					br = new BufferedReader(fr);
					String line;

					while((line=br.readLine())!=null) {

						//コードと名前で分割
						String[] branch = line.split(",");

						//フォーマット確認
						if(!(branch[0].matches("^[0-9]{3}"))) {
							System.out.println("支店定義ファイルのフォーマットが不正です");
							System.exit(0);

						}
						else if(!(branch.length==2)){

							System.out.println("支店定義ファイルのフォーマットが不正です");
							System.exit(0);
						}

						//支店コード
						BranchContents.add(branch[0]);
						//支店名
						BranchContents.add(branch[1]);
						//売上合計金額初期値0
						BranchContents.add("0");
					}

				}catch(IOException e) {
					System.out.println("支店定義ファイルが存在しません");
					System.exit(0);
				}finally {
					if(br != null) {
						try{
							br.close();
						}catch(IOException e) {
							System.out.println("予期せぬエラーが発生しました。");
							System.exit(0);
						}
					}
				}
	}

	//商品定義ファイル読み込み
	public void openproduct(String path) {
				try {
					file = new File(path,"commodity.lst");
					FileReader fr = new FileReader(file);
					br = new BufferedReader(fr);
					String line;

					while((line=br.readLine())!=null) {
						//コードと名前で分割
						String[] branch = line.split(",");
						//フォーマット確認
						if(!(branch[0].matches("^[A-Za-z0-9]{8}"))) {
							System.out.println("商品定義ファイルのフォーマットが不正です");
							System.exit(0);
						}
						else if(!(branch.length==2)){

							System.out.println("商品定義ファイルのフォーマットが不正です");
							System.exit(0);
						}

						//商品コード
						ProductContents.add(branch[0]);
						//商品名
						ProductContents.add(branch[1]);
						//売上合計金額初期値0
						ProductContents.add("0");


					}

				}catch(IOException e) {
					System.out.println("商品定義ファイルが存在しません");
					System.exit(0);
				}finally {
					if(br != null) {
						try{
							br.close();
						}catch(IOException e) {
							System.out.println("予期せぬエラーが発生しました。");
							System.exit(0);
						}
					}
				}
		}


	//売上ファイル名の取得
		public void openrcd(String path) {
			file = new File(path);
			//ファイルの一覧を取得する
			File[] filenames = file.listFiles();
			int i;
			//rcdのファイルを取得
			for(i=0; i<filenames.length; ++i){
				String filename=filenames[i].getName();
				if(filename.contains("rcd")) {
					String[] filenamesp = filename.split(".rcd");
					//ファイル名が8桁の数字かつ拡張子が.rcdのファイル名を取得
					if(filenamesp[0].matches("[0-9]{8}")) {
						Earnings.add(filenamesp[0]);
					}
				}
			}

			//連番か調べる
			int count=0;
			for(i=0; i<Earnings.size();i++) {
				if(i==0) {
					count = Integer.valueOf(Earnings.get(i));
				}else {
					if(Integer.valueOf(Earnings.get(i))== count+1) {
						count = count+1;

					}else {
						//連番じゃない場合、エラーメッセージを表示し処理を終了
						System.out.println("売上ファイル名が連番になっていません");
						System.exit(0);
					}
				}
			}

		}



		//売上ファイルの読み込み及び集計
		public void totalling(String path) {
			//rcdファイル読み込み
			for(i = 0; i<Earnings.size();i++) {
				try {
					file = new File(path,Earnings.get(i)+".rcd");
					FileReader fr = new FileReader(file);
					br = new BufferedReader(fr);
					String line;
					//0番目:支店コード,１番目:商品コード,2番目:売上コード
					List<String> Earningslist = new ArrayList<>();
					while((line=br.readLine())!=null) {
						Earningslist.add(line);
					}

					//売上ファイルの中身が3行ではない場合
					if(!(Earningslist.size()==3)) {
						System.out.println(Earnings.get(i)+".rcdのフォーマットが不正です");
						System.exit(0);

					}

					//支店売上集計

					//支店コード検索
					int id = BranchContents.indexOf(Earningslist.get(0));
					//支店コードが見つからない場合
					if(id == -1) {
						System.out.println(Earnings.get(i)+".rcdの支店コードが不正です");
						System.exit(0);
					}
					//売上計算
					Integer Calculation=0;
					try {
						 Calculation= Integer.valueOf(BranchContents.get(id+2))+Integer.valueOf(Earningslist.get(2));
					}
					//計算できないとき
					catch(Exception e) {
						System.out.println("予期せぬエラーが発生しました");
						System.exit(0);
					}
					//合計金額が10桁を超える場合
					if(Calculation>=1000000000) {
						System.out.println("合計金額が10桁を超えました");
						System.exit(0);
					}
					BranchContents.set(id+2,Calculation.toString());


					//商品売上集計

					//商品コード検索
					id = ProductContents.indexOf(Earningslist.get(1));
					//商品コードが見つからない場合
					if(id == -1) {
						System.out.println(Earnings.get(i)+".rcdの支店コードが不正です");
						System.exit(0);
					}
					//売上計算
					Calculation=0;
					try {
						 Calculation= Integer.valueOf(ProductContents.get(id+2))+Integer.valueOf(Earningslist.get(2));
					}
					//計算できないとき
					catch(Exception e) {
						System.out.println("予期せぬエラーが発生しました");
						System.exit(0);
					}
					//合計金額が10桁を超える場合
					if(Calculation>=1000000000) {
						System.out.println("合計金額が10桁を超えました");
						System.exit(0);
					}
					ProductContents.set(id+2,Calculation.toString());

				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
				}finally {
					if(br != null) {
						try{
							br.close();
						}catch(IOException e) {
							System.out.println("closeできませんでした。");
						}
					}
			}
			}

		}

		public void createfile(String path,String name) {

			file = new File(path+name);

			//支店別集計ファイル作成
			try {
			      if (!(file.createNewFile())) {
			        System.out.println("予期せぬエラーが発生しました");
			        System.exit(0);
			      }
			      FileWriter filewriter = new FileWriter(file);
	    		  i =0;
	    		  //支店別集計ファイルに書き込み
	    		  if(name.equals("/branch.out")) {

			      while(i<BranchContents.size()){
			   		 	String write = BranchContents.get(i)+","+BranchContents.get(i+1)+","+BranchContents.get(i+2);
			   		 	filewriter.write(write+"\n");
			   		 	i=i+3;
			      }}
	    		  //商品別集計ファイルに書き込み
	    		  else {
	    			  while(i<ProductContents.size()){
				   		 	String write = ProductContents.get(i)+","+ProductContents.get(i+1)+","+ProductContents.get(i+2);
				   		 	filewriter.write(write+"\n");
				   		 	i=i+3;
				      }

	    		  }


			      filewriter.close();
			    }catch(IOException e) {
		    		  System.out.println("予期せぬエラーが発生しました");
		    		  System.exit(0);
			    }


		}



}
